%global release_version %%(echo %{version} | awk -F. '{print $1"."$2}')

Name:           libsecret
Version:        0.20.5
Release:        2
Summary:        Library for storing and retrieving passwords and other secrets
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/Libsecret
Source0:        https://download.gnome.org/sources/libsecret/%{release_version}/libsecret-%{version}.tar.xz

BuildRequires:  glib2-devel gobject-introspection-devel intltool vala gettext gi-docgen meson
BuildRequires:  libgcrypt-devel >= 1.2.2 libxslt-devel docbook-style-xsl 
%ifarch "%{valgrind_arches}"
BuildRequires:  valgrind-devel
%endif
Provides:       bundled(egglib)

%description
A GObject-based library for accessing the Secret Service API of the freedesktop.org
project, a cross-desktop effort to access passwords, tokens and other types of secrets.
libsecret provides a convenient wrapper for these methods so consumers do not have to
call the low-level DBus methods.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -p1

%ifarch "%{valgrind_arches}"
rm -rf build/valgrind/
%endif

%build
%meson
%meson_build

%install
%meson_install
%delete_la_and_a

%find_lang libsecret

%ldconfig_scriptlets

%files -f libsecret.lang
%license COPYING
%{_libdir}/libsecret-1.so.0*
%{_libdir}/girepository-1.0/Secret-1.typelib
%{_bindir}/secret-tool

%files devel
%{_includedir}/libsecret-1/
%{_libdir}/libsecret-1.so
%{_libdir}/pkgconfig/libsecret-1.pc
%{_libdir}/pkgconfig/libsecret-unstable.pc
%{_datadir}/gir-1.0/Secret-1.gir
%{_datadir}/vala/vapi/libsecret-1.deps
%{_datadir}/vala/vapi/libsecret-1.vapi

%files help
%doc NEWS README.md
%license docs/reference/COPYING
%doc %{_mandir}/man1/secret-tool.1*
%doc %{_docdir}/libsecret-1

%changelog
* Sun Apr 28 2024 yinsist <jianhui.oerv@isrc.iscas.ac.cn> - 0.20.5-2
- Valgrind does not support certain architectures like RISC-V, Before depending on Valgrind, first check if Valgrind supports the architecture

* Tue Feb 27 2024 dongyuzhen <dongyuzhen@h-partners.com> - 0.20.5-1
- revert "Update to 0.21.0"

* Wed Aug 16 2023 dillon chen <dillon.chen@gmail.com> - 0.21.0-1
- Update to 0.21.0

* Thu Apr 21 2022 dillon chen <dillon.chen@gmail.com> - 0.20.5-1
- Update to 0.20.5

* Tue Jan 26 2021 liudabo <liudabo1@huawei.com> - 0.20.4-1
- upgrade version to 0.20.4

* Mon Jul 27 2020 wenzhanli<wenzhanli2@huawei.com> - 0.20.3-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:version update 0.20.3

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.6-4
- Correct requires

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.6-3
- Format BuildRequires

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.6-2
- Adjust requires

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.18.6-1
- Package init
